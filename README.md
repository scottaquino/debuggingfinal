# **What is B.A.U.T?**

B.A.U.T stands for Botaish-Aquino Unity Test. This tool was designed to allow developers to have the functionality of the conditional 
breakpoint without attaching a compiler to a running version of the game. With this tool, users will be allowed to specify what 
happens when a condition is met along with knowing exactly which object made that condition true. Our tool also creates a list 
that will display every tracked variable in the scene, along with the current value of those variables. 

# **How to Use B.A.U.T?**

## Step 1: Insert the correct debugging attribute before the variables. 

![Scheme](https://i.gyazo.com/9edc8de735e76d5eac0f0158ce2efafb.png)

Inserting this attribute will alert the tool that this variable is being tracked. It is important to note which parameters are being
passed into the function as they decided both what the condition should be along with what event should follow if that conditional 
was met. 

### Parameter 1: Comparison
This variable is a flag to see what comparison the user wants to do on the variable. The following are currently supported flags:

* EQUAL ( == )
* GREATER_THAN ( > )
* GREATER_THAN_EQUAL_TO ( >= )
* LESS_THAN ( < )
* LESS_THAN_EQUAL_TO ( <= )
* BOUNDS ( > && < )
* BOUNDS_EQUAL ( >= && <=)
* NOT_BOUNDS ( < || > )
* NOT_BOUNDS_EQUAL ( <= || >= )
	
### Parameter 2: Event
This variable is a flag to see what happens if the condition specified has been met. The following are currently supported flags

* BREAK (Pauses the editor and highlights the gameobject)
* WARNING (Prints as a warning)
* ERROR  (Prints as an error)
* PRINT (Prints normally)

### Parameter 3: Output Text
This variable is the message you want to be printed once the conditional has been met. This output will be directly to the console 
window. This is an optional parameter; therefore, if the user does not want to print a message, they can simply using null as the 
parameter.

### Parameter 4+: Comparison values
This variable is the values used in the conditional when running the program. The following are currently supported types:

* Int 
* Float
* Double
* String

## Step 2: Using the Tool
Users who want to search the entire scene of variables the contain the debugging attribute can do so by simply clicking the 
"Search all Monobehaviours" checkbox located in the top left-hand corner of the tool's window.

![Scheme](https://i.gyazo.com/930b312cec100d4af2b961c5ef16e0d8.gif)

Users who only want to search a specific object for the debugging attribute can simply change the size of the public array to the number
of objects they want to search, then click and drag the particular objects you want to search into the open slots. 

![Scheme](https://i.gyazo.com/c1d32c758b20522edeabdbfe53f4e810.gif)


# **Further Investigations:**
The point of this project was to give a programmer more debugging tools within Unity. We did not want it to be compiler-specific but more
on the Unity side of game development. Our goal was to copy what a conditional breakpoint would accomplish but with a few more features. 
Overall, this project was a success. We were able to produce a tool that can easily be implemented into Unity to aid in debugging variables. 
Almost as a side product of our tool, we were able to create a version of a "watch" that a programmer would see in Visual Studios. This tool
would have been extremely useful in both of our capstone projects. Although our tool was successful and we accomplished the goals we set 
out to accomplished, that does not mean we can't improve upon this. 

* Make a far more generic tool. One of the problems right now is we have to specify each type since generic templates are not 
  allowed when inheriting from the Attribute class. 
* Make a more customizable tool. Possibly outputting to files. Customizing the colors of the variables that change would 
  distinguish it more from other variables. 
* Show the call stack when asserting. There doesn't seem to be an easy way right now to see the exact point of concern.
  Make it a plugin to utilize the power of C/C++
  
# **Contributions**
## Mark Botaish
* Created the debugging attribute 
* Worked on the UML
* Wrote the ReadMe
* Assisted in the debugging tool 
## Scott Aquino
* Created the debugging tool
* Worked on the UML
* Assisted in the debugging attribute


# **Want to download the repo**?
Click [Here]( https://MarkBotaish@bitbucket.org/scottaquino/debuggingfinal.git) to take you to the reposity, although you may already be here!

