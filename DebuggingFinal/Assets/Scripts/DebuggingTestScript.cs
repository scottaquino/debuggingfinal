﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebuggingTestScript : MonoBehaviour
{
    [Debugging(DebuggingType.LESS_THAN, Handler.PRINT, "This should be less than 30", 30)]
    public int publicTestInt = 5;

    [Debugging(DebuggingType.BOUNDS_EQUAL, Handler.WARNING, "This should be between 0.0 and 0.5", 0.0f, 5f)]
    protected float protectedTestFloat = 0f;

    [Debugging(DebuggingType.EQUAL, Handler.ERROR, "Dan has left you on read", "Peachy")]
    private string privateTestString = "Hi Dan, how are you?";

    [Debugging(DebuggingType.GREATER_THAN, Handler.BREAK, "The value is negative", (double)0)]
    private double privateTestDouble = 40;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(IncrementValues());

        if (privateTestString == null)
            Debug.Log("hey");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator IncrementValues()
    {
        yield return new WaitForSeconds(0.5f);
        ++publicTestInt;
        if (protectedTestFloat >= 6)
            --protectedTestFloat;
        else
            ++protectedTestFloat;
        --privateTestDouble;

        StartCoroutine(IncrementValues());
    }
}
