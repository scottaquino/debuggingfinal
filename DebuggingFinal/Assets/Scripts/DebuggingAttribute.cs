﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System;

public enum DebuggingType
{
    EQUAL, 
    GREATER_THAN,
    GREATER_THAN_EQUAL_TO,
    LESS_THAN,
    LESS_THAN_EQUAL_TO,     
    BOUNDS,
    BOUNDS_EQUAL,
    NOT_BOUNDS, 
    NOT_BOUNDS_EQUAL
}

public enum Handler
{
    BREAK, 
    WARNING,
    ERROR, 
    PRINT
}

[AttributeUsage(AttributeTargets.Field)]
public class DebuggingAttribute : Attribute
{
    private enum TheType
    {
        INT,
        FLOAT,
        DOULBE,
        STRING
    }


    public readonly DebuggingType type;
    public readonly Handler handler;
    public readonly string debugLogMessage = "";

    private TheType theType;

    public readonly int[] intVars;
    public readonly float[] floatVars;
    public readonly double[] doubleVars;
    public readonly string[] stringVars;

   /// <summary>
   /// Constructor
   /// </summary>
   /// <param name="type"> Conditional statement</param>
   /// <param name="handler"> Event to call once condition is met </param>
   /// <param name="list"> List of parameters the conditional will use. MAX should be size 2 </param>
    public DebuggingAttribute(DebuggingType type, Handler handler, string logMessage,  params int[] list)
    {
        this.type = type;
        this.handler = handler;
        intVars = list;
        theType = TheType.INT;

        if (logMessage != null)
            this.debugLogMessage = "Log: " + logMessage;
        else
            this.debugLogMessage = "";
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="type"> Conditional statement</param>
    /// <param name="handler"> Event to call once condition is met </param>
    /// <param name="list"> List of parameters the conditional will use. MAX should be size 2 </param>
    public DebuggingAttribute(DebuggingType type, Handler handler, string logMessage, params float[] list)
    {
        this.type = type;
        this.handler = handler;
        floatVars = list;
        theType = TheType.FLOAT;

        if (logMessage != null)
            this.debugLogMessage = "Log: " + logMessage;
        else
            this.debugLogMessage = "";
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="type"> Conditional statement</param>
    /// <param name="handler"> Event to call once condition is met </param>
    /// <param name="list"> List of parameters the conditional will use. MAX should be size 2 </param>
    public DebuggingAttribute(DebuggingType type, Handler handler, string logMessage, params double[] list)
    {
        this.type = type;
        this.handler = handler;
        doubleVars = list;
        theType = TheType.DOULBE;

        if (logMessage != null)
            this.debugLogMessage = "Log: " + logMessage;
        else
            this.debugLogMessage = "";
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="type"> Conditional statement</param>
    /// <param name="handler"> Event to call once condition is met </param>
    /// <param name="list"> List of parameters the conditional will use. MAX should be size 2 </param>
    public DebuggingAttribute(DebuggingType type, Handler handler, string logMessage, params string[] list)
    {
        this.type = type;
        this.handler = handler;
        stringVars = list;
        theType = TheType.STRING;

        if (logMessage != null)
            this.debugLogMessage = "Log: " + logMessage;
        else
            this.debugLogMessage = ""; ;
    }

    /// <summary>
    /// Convert the object to the correct type and run the comparison
    /// </summary>
    /// <param name="value"> The object to compare</param>
    /// <returns></returns>
    public bool AssertValue(object value)
    {
        bool pass = false;

        switch (theType)
        {
            case TheType.FLOAT:
                pass = TestValue((float)value);
                break;
            case TheType.INT:
                pass = TestValue((int)value);
                break;
            case TheType.DOULBE:
                pass = TestValue((double)value);
                break;
            case TheType.STRING:
                pass = TestValue((string)value);
                break;
        }

        if (!pass && Application.isPlaying)
        {
            switch (handler)
            {
                case Handler.PRINT:
                    Debug.Log("Invalid value: " + value.ToString() + ". " + debugLogMessage);
                    break;
                case Handler.WARNING:
                    Debug.LogWarning("Invalid value: " + value.ToString() + ". " + debugLogMessage);
                    break;
                case Handler.ERROR:
                    Debug.LogError("Invalid value: " + value.ToString() + ". " + debugLogMessage);
                    break;
                case Handler.BREAK:
                    Debug.LogError("Invalid value: " + value.ToString() + ". " + debugLogMessage);
                    Debug.Break();
                    break;
            }
        }


        return pass;
    }


    /// <summary>
    /// Compares the value to the specified conditions/parameters
    /// </summary>
    /// <param name="value"> Value to compare </param>
    /// <returns>Returns true if the condition has not been met. Returns false if the conditions has been met</returns>
    private bool TestValue(int value)
    {
        switch (type)
        {
            case DebuggingType.EQUAL:
                return (value == intVars[0]);
            case DebuggingType.GREATER_THAN:
                return (value > intVars[0]);
            case DebuggingType.GREATER_THAN_EQUAL_TO:
                return (value >= intVars[0]);
            case DebuggingType.LESS_THAN:
                return (value < intVars[0]);
            case DebuggingType.LESS_THAN_EQUAL_TO:
                return (value <= intVars[0]);
            case DebuggingType.BOUNDS:
                return (value > intVars[0] && value < intVars[1]);
            case DebuggingType.BOUNDS_EQUAL:
                return (value >= intVars[0] && value <= intVars[1]);
            case DebuggingType.NOT_BOUNDS:
                return (value < intVars[0] || value > intVars[1]);
            case DebuggingType.NOT_BOUNDS_EQUAL:
                return (value <= intVars[0] || value >= intVars[1]);
            default:
                return false;
        }
    }
    /// <summary>
    /// Compares the value to the specified conditions/parameters
    /// </summary>
    /// <param name="value"> Value to compare </param>
    /// <returns>Returns true if the condition has not been met. Returns false if the conditions has been met</returns>
    private bool TestValue(float value)
    {
        switch (type)
        {
            case DebuggingType.EQUAL:
                return (value == floatVars[0]);
            case DebuggingType.GREATER_THAN:
                return (value > floatVars[0]);
            case DebuggingType.GREATER_THAN_EQUAL_TO:
                return (value >= floatVars[0]);
            case DebuggingType.LESS_THAN:
                return (value < floatVars[0]);
            case DebuggingType.LESS_THAN_EQUAL_TO:
                return (value <= floatVars[0]);
            case DebuggingType.BOUNDS:
                return (value > floatVars[0] && value < floatVars[1]);
            case DebuggingType.BOUNDS_EQUAL:
                return (value >= floatVars[0] && value <= floatVars[1]);
            case DebuggingType.NOT_BOUNDS:
                return (value < floatVars[0] || value > floatVars[1]);
            case DebuggingType.NOT_BOUNDS_EQUAL:
                return (value <= floatVars[0] || value >= floatVars[1]);
            default:
                return false;
        }
    }

    /// <summary>
    /// Compares the value to the specified conditions/parameters
    /// </summary>
    /// <param name="value"> Value to compare </param>
    /// <returns>Returns true if the condition has not been met. Returns false if the conditions has been met</returns>
    private bool TestValue(double value)
    {
        switch (type)
        {
            case DebuggingType.EQUAL:
                return (value == doubleVars[0]);
            case DebuggingType.GREATER_THAN:
                return (value > doubleVars[0]);
            case DebuggingType.GREATER_THAN_EQUAL_TO:
                return (value >= doubleVars[0]);
            case DebuggingType.LESS_THAN:
                return (value < doubleVars[0]);
            case DebuggingType.LESS_THAN_EQUAL_TO:
                return (value <= doubleVars[0]);
            case DebuggingType.BOUNDS:
                return (value > doubleVars[0] && value < doubleVars[0]);
            case DebuggingType.BOUNDS_EQUAL:
                return (value >= doubleVars[0] && value <= doubleVars[1]);
            case DebuggingType.NOT_BOUNDS:
                return (value < doubleVars[0] || value > doubleVars[1]);
            case DebuggingType.NOT_BOUNDS_EQUAL:
                return (value <= doubleVars[0] || value >= doubleVars[1]);
            default:
                return false;
        }
    }

    /// <summary>
    /// Compares the value to the specified conditions/parameters
    /// </summary>
    /// <param name="value"> Value to compare </param>
    /// <returns>Returns true if the condition has not been met. Returns false if the conditions has been met</returns>
    private bool TestValue(string value)
    {
        switch (type)
        {
            case DebuggingType.EQUAL:
                return (value == stringVars[0]);
            default:
                return false;
        }
    }
}
