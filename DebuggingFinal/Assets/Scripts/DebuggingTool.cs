﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

public class DebuggingTool : EditorWindow
{
    /// <summary>
    /// Class to store data about debugging varialbes
    /// </summary>
    private class VariableData
    {
        public VariableData(string str, object obj, bool b = false)
        {
            name = str;
            value = obj;
            pass = b;
        }

        public string name;
        public object value;
        public bool pass;
    }

    private List<VariableData> variableList = new List<VariableData>();

    GameObject[] viewedObject = new GameObject[999];
    int arraySize = 0;
    bool searchAll = false;
    bool isBroken = false;
    Vector2 scrollPos;

    // Add menu item named "My Window" to the Window menu
    [MenuItem("Window/B.A.U.T")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        GetWindow(typeof(DebuggingTool), false, "B.A.U.T");
    }

    /// <summary>
    /// Update the GUI for the tool
    /// </summary>
    void OnGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(position.width), GUILayout.Height(position.height));
        searchAll = EditorGUILayout.ToggleLeft("Search all Monobehaviours", searchAll);

        List<GameObject> checkedObjs = new List<GameObject>();

        // Get all monobehaviours and check accordingly
        if (searchAll)
        {
            MonoBehaviour[] sceneActive = FindObjectsOfType<MonoBehaviour>();
            int i = 0;

            foreach (MonoBehaviour mono in sceneActive)
            {
                if (!checkedObjs.Contains(mono.gameObject))
                {
                    DrawDebugger(i, mono.gameObject);
                    checkedObjs.Add(mono.gameObject);
                    ++i;
                }
            }
        }
        else
        {
            arraySize = EditorGUILayout.IntField(arraySize);

            for (int i = 0; i < arraySize; ++i)
                DrawDebugger(i);
        }

        EditorGUILayout.EndScrollView();
    }

    /// <summary>
    /// Draw information on window
    /// </summary>
    /// <param name="index"> Index of object to check from list </param>
    /// <param name="obj"> Object that is being checked </param>
    private void DrawDebugger(int index, GameObject obj = null)
    {
        variableList.Clear();

        GUILayout.Label("----------Watch " + index.ToString() + "----------", EditorStyles.boldLabel);
        GUILayout.Space(5);

        if (searchAll)
        {
            obj = (GameObject)EditorGUILayout.ObjectField(obj, typeof(UnityEngine.Object), true);
            CheckVars(obj);
        }
        else
        {
            viewedObject[index] = (GameObject)EditorGUILayout.ObjectField(viewedObject[index], typeof(UnityEngine.Object), true);
            CheckVars(viewedObject[index]);
        }

        GUILayout.Space(5);
    }

    /// <summary>
    /// Check the variabels and test their value against their parameters
    /// </summary>
    /// <param name="obj"> Gameobject to test </param>
    private void CheckVars(GameObject obj)
    {
        GUIStyle style = new GUIStyle();
        if (obj)
        {
            foreach (MonoBehaviour mono in obj.GetComponents<MonoBehaviour>())
            {
                style.normal.textColor = Color.blue;
                GUILayout.Label("-------" + mono.ToString() + "-------", style);

                Type monoType = mono.GetType();

                // Find all instances to check from the monobehaviour
                FieldInfo[] objectFields = monoType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Default);

                // Find all variables to check for each monobehaviour
                for (int i = 0; i < objectFields.Length; i++)
                {
                    DebuggingAttribute attribute = Attribute.GetCustomAttribute(objectFields[i], typeof(DebuggingAttribute)) as DebuggingAttribute;

                    // If an attribute is found, check it
                    if (attribute != null)
                    {
                        bool pass = attribute.AssertValue(objectFields[i].GetValue(mono));

                        if (EditorApplication.isPaused && !isBroken)
                        {
                            EditorGUIUtility.PingObject(mono);
                            isBroken = true;
                        }

                        variableList.Add(new VariableData(objectFields[i].Name, objectFields[i].GetValue(mono), pass));
                    }
                }

                // Print out data based on pass/fail
                for (int i = 0; i < variableList.Count; ++i)
                {
                    if (variableList.Count > i)
                    {
                        if (variableList[i].pass)
                        {

                            style.normal.textColor = Color.black;
                            style.fontSize = 12;
                            variableList[i].value = EditorGUILayout.TextField(variableList[i].name, variableList[i].value.ToString(), style);
                        }
                        else
                        {

                            style.normal.textColor = Color.red;
                            style.fontSize = 12;
                            variableList[i].value = EditorGUILayout.TextField(variableList[i].name, variableList[i].value.ToString(), style);
                        }
                    }
                }

                // Reset variables for next round
                variableList.Clear();
            }
        }
    }

    /// <summary>
    /// Update that runs synchronus to editor update
    /// </summary>
    private void Update()
    {
         Repaint();

        if (!EditorApplication.isPaused && isBroken)
            isBroken = false;
    }
}
