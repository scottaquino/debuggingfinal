﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnotherTestScript : MonoBehaviour
{
    [Debugging(DebuggingType.EQUAL, Handler.ERROR, "Dan hates you and will not say hi back", "Dan says hi")]
    private string danMessage = "No";

    // Start is called before the first frame update
    void Start()
    {
        danMessage = "Dan says hi";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
