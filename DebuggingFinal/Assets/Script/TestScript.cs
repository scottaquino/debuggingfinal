﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;

public class TestScript : MonoBehaviour
{
    [Debugging(DebuggingType.GREATER_THAN_EQUAL_TO, Handler.WARNING, null, 1)]
    public int ImAVariable = 0;

    [Debugging(DebuggingType.LESS_THAN_EQUAL_TO, Handler.ERROR, null, 0f)]
    public float ImAVariable2 = 0;

    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
